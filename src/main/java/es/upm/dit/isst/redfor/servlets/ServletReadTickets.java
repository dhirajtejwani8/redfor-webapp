package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Ticket;

@WebServlet("/ServletReadTickets")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
public class ServletReadTickets extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Client c = ClientBuilder.newClient(new ClientConfig());

		List<Ticket> t = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets").request()
				.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Ticket>>() {
				});

		req.setAttribute("t", t);

		// ver si hay activos para saber si dibujar la tabla en MapaTickets.jsp o no
		boolean hayActivos = false;
		for (Ticket ti : t) {
			if (ti.isActivo() == true) {
				hayActivos = true;
				break;
			}
		}

		req.setAttribute("hayActivos", hayActivos);
		getServletContext().getRequestDispatcher("/MapaTickets.jsp").forward(req, resp);
	}
}
