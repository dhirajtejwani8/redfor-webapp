package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Agente;
import es.upm.dit.isst.redfor.model.Nodo;

@WebServlet("/ServletReadAgentes")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
public class ServletReadAgentes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Client c = ClientBuilder.newClient(new ClientConfig());
		List<Agente> a = c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes").request()
				.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Agente>>() {
				});
		req.setAttribute("a", a);

		String fate = req.getParameter("fate");
		if (fate.equals("createTicket")) {

			c = ClientBuilder.newClient(new ClientConfig());
			List<Nodo> n = c.target("http://localhost:8080/REDFOR-SERVICE/rest/nodos").request()
					.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Nodo>>() {
					});
			req.setAttribute("n", n);

			getServletContext().getRequestDispatcher("/CreateTicket.jsp").forward(req, resp);

		} else if (fate.equals("updateAgente")) {

			getServletContext().getRequestDispatcher("/UpdateAgente.jsp").forward(req, resp);
		}
	}

}
