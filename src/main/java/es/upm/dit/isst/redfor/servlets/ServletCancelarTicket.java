package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Ticket;
import es.upm.dit.isst.redfor.model.Agente;

@WebServlet("/ServletCancelarTicket")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
public class ServletCancelarTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		if (req.isUserInRole("ac")) {

			getServletContext().getRequestDispatcher("/AgenteCampo.jsp").forward(req, resp);

		} else {

			int idTicket = Integer.parseUnsignedInt((req.getParameter("idTicket")));
			Client c = ClientBuilder.newClient(new ClientConfig());

			/** cambiar el atributo activo de ticket a false */
			Ticket ticket = null;
			try {
				ticket = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket).request()
						.accept(MediaType.APPLICATION_JSON).get(Ticket.class);

			} catch (Exception e) {
			}
			if (ticket != null) {
				// agentes ya no disponibles
				Agente agente = null;
				for (String emailAgenteAsignado : ticket.getEmailsAgentesAsignados()) {
					try {
						agente = c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + emailAgenteAsignado)
								.request().accept(MediaType.APPLICATION_JSON).get(Agente.class);
					} catch (Exception e) {
						throw e;
					}
					if (agente != null) {
						agente.setDisponible(true);
						c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + emailAgenteAsignado).request()
								.post(Entity.entity(agente, MediaType.APPLICATION_JSON), Response.class);
					}

				}
				// ticket ya no activo y no hay agentes asignados
				ticket.setActivo(false);
				ticket.setEmailsAgentesAsignados(null);
				c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets/" + idTicket).request()
						.post(Entity.entity(ticket, MediaType.APPLICATION_JSON), Response.class);

			}

			/** para volver atrás leemos todos los tickets */
			List<Ticket> t = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets").request()
					.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Ticket>>() {
					});
			req.setAttribute("t", t);
			/**
			 * ver si hay activos para no dibujar la tabla si es vacía en MapaTickets.jsp
			 */
			boolean hayActivos = false;
			for (Ticket ti : t) {
				if (ti.isActivo() == true) {
					hayActivos = true;
					break;
				}
			}
			req.setAttribute("hayActivos", hayActivos);
			getServletContext().getRequestDispatcher("/MapaTickets.jsp").forward(req, resp);
		}

	}

}
