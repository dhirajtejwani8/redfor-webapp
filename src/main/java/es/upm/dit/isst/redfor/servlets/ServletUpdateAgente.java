package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import es.upm.dit.isst.redfor.model.Agente;

@WebServlet("/ServletUpdateAgente")
@DeclareRoles("aeb")
@ServletSecurity(@HttpConstraint(rolesAllowed = "aeb"))
public class ServletUpdateAgente extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = "";
		try {
			email = req.getParameter("email");
		} catch (Exception e) {
			throw e;
		}
		Client c = ClientBuilder.newClient(new ClientConfig());
		Agente ai = null;
		try {
			ai = c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + email).request()
					.accept(MediaType.APPLICATION_JSON).get(Agente.class);
		} catch (Exception e) {
			throw e;
		}
		if (ai != null) {
			ai.setNombreApellidos(req.getParameter("nombreApellidos"));
			ai.setOficio(req.getParameter("oficio"));
			ai.setPassword(req.getParameter("password"));
			ai.setRol(req.getParameter("rol"));
			ai.setTelefono(Integer.parseUnsignedInt(req.getParameter("telefono")));

			c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes/" + email).request()
					.post(Entity.entity(ai, MediaType.APPLICATION_JSON), Response.class);
		}

		List<Agente> a = c.target("http://localhost:8080/REDFOR-SERVICE/rest/agentes").request()
				.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Agente>>() {
				});
		req.setAttribute("a", a);
		getServletContext().getRequestDispatcher("/UpdateAgente.jsp").forward(req, resp);

	}

}
