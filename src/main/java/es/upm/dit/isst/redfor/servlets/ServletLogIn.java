package es.upm.dit.isst.redfor.servlets;

import java.io.IOException;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.isst.redfor.model.Ticket;

@WebServlet("/ServletLogIn")
@DeclareRoles({ "aeb", "ac" })
@ServletSecurity(@HttpConstraint(rolesAllowed = { "aeb", "ac" }))
public class ServletLogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		if (req.isUserInRole("aeb")) {

			getServletContext().getRequestDispatcher("/MapaZonas.jsp").forward(req, resp);

		} else {

			String email = req.getUserPrincipal().getName();

			Client c = ClientBuilder.newClient(new ClientConfig());

			List<Ticket> t = c.target("http://localhost:8080/REDFOR-SERVICE/rest/tickets").request()
					.accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Ticket>>() {
					});

			Ticket ticket = null;
			for (Ticket ti : t) {
				for (String emailAgenteAsignado : ti.getEmailsAgentesAsignados()) {
					if (emailAgenteAsignado.equals(email)) {
						ticket = ti;
						break;
					}
				}
				if (ticket != null) {
					break;
				}
			}
			req.setAttribute("ticket", ticket);

			getServletContext().getRequestDispatcher("/AgenteCampo.jsp").forward(req, resp);
		}

	}

}
