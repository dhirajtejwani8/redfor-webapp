<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>CrearNodo.jsp</title>
		<link rel="stylesheet" href="css/CreateNodo.css"/>
	</head>
	<body>
	
		<h1>Crear Nodo</h1>
		
		<form action="ServletCreateNodo">
			<input type="text" name="latitud" placeholder="Latitud">
			<br><br>
			<input type="text" name="longitud" placeholder="Longitud">
			<br><br>
			<input type="submit" value="Crear Nodo"/>
			<br><br>
		</form>
		
		<form action="ServletReadTickets">
			<input type="submit" value="Volver Atrás"/>
		</form>
		<br>
		
		<%@ include file="LogOut.jsp"%>
		
	</body>
</html>