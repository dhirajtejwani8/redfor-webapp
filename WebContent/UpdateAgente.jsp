<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title>UpdateAgente.jsp</title>
		<link rel="stylesheet" href="css/UpdateAgente.css"/>
		
	</head>
	<body>
		<h1>Update Agentes</h1>
		<button onclick="document.location.href='Admin.jsp'">Volver Atrás</button>
		<br>
		<br>
				
		<c:if test="${empty a}">
			<span>No hay agentes creados en el sistema.</span>
			<br><br>
		</c:if>

		<table border="1">
		
			<tr>
				<th>Email</th>
				<th>Nombre y Apellidos</th>
				<th>Oficio</th>
				<th>Contraseña</th>
				<th>Rol</th>
				<th>Teléfono</th>
				<th>Modificar</th>
			</tr>
			
			<c:forEach items="${a}" var="ai">
			
				<form action="ServletUpdateAgente">
					
					<tr>
						<td>${ai.email}<input type="hidden" name="email" value="${ai.email}"/></td>
						<td><input type="text" name="nombreApellidos" value="${ai.nombreApellidos}"></td>
						<td><input type="text" name="oficio" value="${ai.oficio}"/></td>
						<td><input type="password" name="password" value="${ai.password}"/></td>
						
						<td>
							<select name="rol">
								<c:if test="${ai.rol=='ac'}">
									<option value="ac">Agente de Campo</option>
									<option value="aeb">Agente de Estación Base</option>
								</c:if>
								<c:if test="${ai.rol=='aeb'}">
									<option value="aeb">Agente de Estación Base</option>
									<option value="ac">Agente de Campo</option>
								</c:if>
							</select>
						</td>
						
						<td><input type="number" name="telefono" min="600000000" max="999999999" value="${ai.telefono}"/></td>
						<td><input type="submit" value="Update Agente"/></td>
						
					</tr>
					
				</form>
				<br>
			</c:forEach>
			
		</table>
		
		<%@ include file="LogOut.jsp"%>
	</body>
</html>