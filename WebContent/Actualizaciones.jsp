<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html lang="es-ES">

	<head>
		<meta charset="UTF-8">
		<title>Actualizaciones.jsp</title>
		<link rel="stylesheet" href="css/styles.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	
	<body>
			
			
				
		<h1>Actualizaciones</h1>
		
		
		
		<form action="ServletCreateActualizacion" method="post" enctype="multipart/form-data">
			<input type="text" id="descripcion" name="descripcion" placeholder="Descripción..."/>
			<input type="file" id="foto" name="foto"/>
			
			<input type="hidden" name="idTicket" value="${idTicket}"/>
			<input type="submit" value="Añadir Nueva Actualización"/>
		</form>
		<br>
		
		

		<form action="ServletReadActualizaciones">
			<input type="hidden" name="idTicket" value="${idTicket}"/>
			<input type="submit" value="Actualizar Página"/>
		</form>
	
	
	
		<!-- volver atrás -->
		<c:if test="${rol=='aeb'}">
			<form action="ServletReadTickets">
				<input type="submit" value="Volver Atrás"/>
			</form>
		</c:if>
		<c:if test="${rol=='ac'}">
			<form action="ServletLogIn">
				<input type="submit" value="Volver Atrás"/>
			</form>
		</c:if>
		<br>
		
		
		
		<%@ include file="LogOut.jsp"%>
		<br>

		
		
		<!-- mostrar actualizaciones si las hay-->
		<c:if test="${empty a}">
			<span>No hay actualizaciones de momento para este ticket.</span>
			<br><br>
		</c:if>
		<c:forEach var="i" begin="1" end="${fn:length(a)}">
			<div id="actualizacion">
			
				<jsp:useBean id="dateObject" class="java.util.Date"/>
				<jsp:setProperty name="dateObject" property="time" value="${a[i-1].timestamp}"/>
				<fmt:formatDate value="${dateObject}" pattern="hh:mm dd-MMM-yy"/>
			
				<span>${a[i-1].emailAgenteAutor}</span>
				<br><br>
				
				<span>${a[i-1].descripcion}</span>
			
				<c:if test="${filePaths[i-1]!='hola'}">
					<br>
					<img src="images/${filePaths[i-1]}"/>
				</c:if>		
				
			</div>
			<br>
		</c:forEach>

		
		
		<!-- cancelar ticket-->
		<form action="ServletCancelarTicket">
			<input type="hidden" name="idTicket" value="${idTicket}"/>
			<input type="submit" value="Cancelar Ticket">
		</form>
		<br>
		
		
		
	</body>
	
</html>