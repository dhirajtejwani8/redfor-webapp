<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Admin.jsp</title>
	<link rel="stylesheet" href="css/Admin.css"/>
</head>
<body>
	<div>
		<button onclick="document.location.href='CreateAgente.jsp'">Registrar Agente</button>
		<br><br>
	
		<form action="ServletReadAgentes">
			<input type="hidden" name="fate" value="updateAgente"/>
			<input type="submit" value="Update Agentes"/>
		</form>
		<br>
	
		<button onclick="document.location.href='MapaZonas.jsp'">Volver Atrás</button>
		<br><br>
		
		<%@ include file="LogOut.jsp"%>
	</div>
</body>
</html>