<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title>CrearTicket.jsp</title>
		<link rel="stylesheet" href="css/CreateTicket.css"/>
	</head>
	<body>
		<h1>Crear Ticket</h1>
		
		
		<div class="form">
			<form action="ServletCreateTicket">
						
				<input type="text" name="titulo" placeholder="Título"><br><br>
				<input type="radio" name="tipo" value="Conato"/>Conato de Incendio
				<input type="radio" name="tipo" value="Alto"/>Alto Riesgo
				<input type="radio" name="tipo" value="Medio"/>Medio Riesgo
				<input type="radio" name="tipo" value="Bajo"/>Bajo Riesgo
				<input type="radio" name="tipo" value="Mantenimiento"/>Mantenimiento<br><br>
				<select name="idNodo">
					<c:forEach items="${n}" var="ni">
						<option value="${ni.id}">Nodo ${ni.id}</option>
					</c:forEach>
				</select>
				<br><br>
				<input type="text" name="descripcion" placeholder="Descripción..."/><br><br>			
				
				<!-- seleccionar uno o varios de los agentes disponibles -->
				<b>Agentes de campo disponibles:</b>
				<br>
				<!-- se presentan solo los que están disponibles y los que son de campo -->
				<c:forEach items="${a}" var="ai">
					<c:if test="${ai.rol!='aeb'}">
						<c:if test="${ai.disponible}">
							<input type="checkbox" name="emailsAgentesAsignados" value="${ai.email}"/>${ai.nombreApellidos}: ${ai.oficio}	
							<br>
						</c:if>
					</c:if>
				</c:forEach>
				<br>
				
			
				<input type="submit" value="Crear Ticket"/><br><br>
		
			</form>
		</div>


		<div class="form">
			<form action="ServletReadTickets">
				<button type="submit">Volver Atrás</button>
			</form>
			<br>
		</div>
		
		
		<%@ include file="LogOut.jsp"%>
		
		
	</body>
</html>