<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<title>MapaTickets.jsp</title>
	<link rel="stylesheet" href="css/MapaTickets.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
</head>
<body>
	<div id="mapa_y_tickets">

		<div id="mapa"></div>

		<div id="tickets">
			<br><span>Hacer click sobre los iconos proporciona datos meteorológicos de municipios o datos de los nodos Arduino.</span>
			<br><br>
			<span id="test"></span>



			<h2>Tickets Activos</h2>

			<c:if test="${hayActivos==false}">
				<span>No hay tickets activos de momento.</span>
				<br>
			</c:if>

			<c:if test="${hayActivos==true}">
				<table border="1">
					<tr>
						<th>ID</th>
						<th>Hora y Fecha</th>
						<th>ID Nodo</th>
						<th>Activo</th>
						<th>Título</th>
						<th>Detalles</th>
					</tr>
					<c:forEach items="${t}" var="ti">
						<c:if test="${ti.activo==true}">
							<tr>
								<td>${ti.id}</td>

								<td><jsp:useBean id="dateObject" class="java.util.Date" />
									<jsp:setProperty name="dateObject" property="time"
										value="${ti.timestamp}" /> <fmt:formatDate
										value="${dateObject}" pattern="hh:mm dd-MMM-yy" /></td>

								<td>${ti.nodo.id}</td>
								<td>${ti.activo}</td>
								<td>${ti.titulo}</td>

								<td>
									<form action="ServletReadActualizaciones">
										<input type="hidden" name="idTicket" value="${ti.id}" /> <input
											type="submit" value="Ver Detalles" />
									</form>
								</td>
							</tr>
						</c:if>
					</c:forEach>
				</table>
			</c:if>
			

			<form style="display: inline;" action="ServletReadAgentes">
				<input type="hidden" name="fate" value="createTicket" />
				<input type="submit" value="Crear Ticket" />
			</form>
			<button onclick="location.href='CreateNodo.jsp'">Crear Nodo</button>
			<button onclick="location.href='MapaZonas.jsp'">Volver Atrás</button>
			
			<%@ include file="LogOut.jsp"%>
			
	<a href="https://icons8.com/icon/FkQHNSmqWQWH/green-circle">Green Circle icon by Icons8</a>
	<a href="https://icons8.com/icon/VW9mAoyk46FP/yellow-circle">Yellow Circle icon by Icons8</a>
	<a href="https://icons8.com/icon/Zyo5wDjgJxRW/red-circle">Red Circle icon by Icons8</a>
	<a href="https://icons8.com/icon/Eu0EFQS62QuU/hammer-and-wrench">Hammer And Wrench icon by Icons8</a>

	<a href="https://icons8.com/icon/5tH5sHqq0t2q/warning">Warning icon by Icons8</a>

	<a href="https://icons8.com/icon/wOBKIsQwmbtX/green-square">Green Square icon by Icons8</a>
	<a href="https://icons8.com/icon/8W74l347DWO4/yellow-square">Yellow Square icon by Icons8</a>
	<a href="https://icons8.com/icon/amr4dGCohyBU/red-square">Red Square icon by Icons8</a>


		</div>
	</div>

	<script>
		$(document).ready(function(){
	
			provincia="Madrid";
				
			$.ajax({
				url:"https://public.opendatasoft.com/api/records/1.0/search/?dataset=provincias-espanolas&rows=52",
				type:"get",
				dataType:"JSON",
				success:function(provincias){

					var records=provincias.records;
					var record=null;
					for(i=0;i<records.length;i++){
						if(records[i].fields.texto==provincia){
							record=records[i];
						}
					}
					var centro_record=record.fields.geo_point_2d;
					var map=L.map('mapa').setView([centro_record[0],centro_record[1]],9);
					L.tileLayer('https://api.maptiler.com/maps/topographique/{z}/{x}/{y}.png?key=76ngdVexoAYKztyaW1fN',{
						tileSize:512,
						zoomOffset:-1,
						minZoom:1,
						attribution:"\u003ca href=\"https://www.maptiler.com/copyright/\" target=\"_blank\"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e",
						crossOrigin:true
					}).addTo(map);

					//eclipse browser doesn't like JS arrow functions:
					//latlngs.forEach(i=>{i.reverse()});
					//latlngs.forEach(function(i){return i.reverse()});

					var geojsonFeature=record.fields.geo_shape;
					L.geoJSON(geojsonFeature,{
						color:"#808080",
						weight:4,
						fill:false
					}).addTo(map);
					
					//definición iconos
					//las de nodos
					var icon_riesgo_bajo=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/yellow-circle-emoji.png",
						iconSize:[30,30]
					});
					var icon_riesgo_medio=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/red-circle-emoji.png",
						iconSize:[40,40]
					});
					var icon_riesgo_alto=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/warning-emoji.png",
						iconSize:[50,50]
					});

					var icon_peace=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/green-circle-emoji.png",
						iconSize:[20,20]
					});
					
					var icon_mantenimiento=L.icon({
						iconUrl:"https://img.icons8.com/emoji/48/000000/hammer-and-wrench.png",
						iconSize:[30,30]
					});
					
					
					
					//las de muni
				
					
					
					var icon_muni_bajo=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/yellow-square-emoji.png",
						iconSize:[30,30]
					});
					var icon_muni_medio=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/red-square-emoji.png",
						iconSize:[40,40]
					});
					var icon_muni_alto=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/warning-emoji.png",
						iconSize:[20,20]
					});
					var icon_muni_peace=L.icon({
						iconUrl:"https://img.icons8.com/emoji/50/000000/green-square-emoji.png",
						iconSize:[20,20]
					});
					

					
					//coger los nodos de bd
					$.ajax({
						url:"http://localhost:8080/REDFOR-SERVICE/rest/nodos",
						type:"get",
						dataType:"JSON",
						cache:false,
						success:function(data){
							var nodos=[];

							for(i=0;i<data.length;i++){
								
								nodos[i]=data[i];
							}
							
							for(i=0;i<nodos.length;i++){
		
								//coger última lectura de cada nodo
								$.ajax({
									url:"http://localhost:8080/REDFOR-SERVICE/rest/nodos/"+nodos[i].id+"/lecturas",
									type:"get",
									dataType:"JSON",
									cache:false,
									success:function(lecturas){

										if(lecturas.length==0){
										}else{
											indice=lecturas.length-1;
											
											$.ajax({
												url:"http://localhost:8080/REDFOR-SERVICE/rest/nodos/"+lecturas[indice].idNodo,
												type:"get",
												dataType:"JSON",
												cache:false,
												success:function(nodo){
													
													var temp=lecturas[indice].temperatura;
													var hum=lecturas[indice].humedad;
													var viento=lecturas[indice].viento;
													var ts=new Date(lecturas[indice].timestamp);
													
													console.log(lecturas[indice].bateriaBaja)
													
													if(lecturas[indice].bateriaBaja==true){
														L.marker([nodo.latitud,nodo.longitud],{
															icon: icon_mantenimiento}).addTo(map).bindPopup(
																"Nodo "+nodo.id+" NECESITA MANTENIMIENTO<br>"
																+"Temperatura: "+temp+" °C<br>"
																+"Humedad: "+hum+" %<br>"
																+"Viento: "+viento+" km/h<br>"
																+"Fecha y Hora de Datos: "+ts
																).openPopup();
													}else if( ((temp>=30)&&(viento<30)&&(hum>=30)) || ((temp<30)&&(viento>=30)&&(hum>=30)) || ((temp<30)&&(viento<30)&&(hum<30)) ){
														L.marker([nodo.latitud,nodo.longitud],{
															icon: icon_riesgo_bajo}).addTo(map).bindPopup(
																"Nodo "+nodo.id+"<br>"
																+"Temperatura: "+temp+" °C<br>"
																+"Humedad: "+hum+" %<br>"
																+"Viento: "+viento+" km/h<br>"
																+"Fecha y Hora de Datos: "+ts
																).openPopup();
													}else if( ((temp>=30)&&(viento>=30)&&(hum>=30)) || ((temp<30)&&(viento>=30)&&(hum<30)) || ((temp>=30)&&(viento<30)&&(hum<30)) ){
														L.marker([nodo.latitud,nodo.longitud],{
															icon: icon_riesgo_medio}).addTo(map).bindPopup(
																"Nodo "+nodo.id+"<br>"
																+"Temperatura: "+temp+" °C<br>"
																+"Humedad: "+hum+" %<br>"
																+"Viento: "+viento+" km/h<br>"
																+"Fecha y Hora de Datos: "+ts
																).openPopup();
														//se cumplen las 3 30s
													}else if((temp>=30)&&(viento>=30)&&(hum<30)){
														L.marker([nodo.latitud,nodo.longitud],{
															icon: icon_riesgo_alto}).addTo(map).bindPopup(
																"Nodo "+nodo.id+"<br>"
																+"Temperatura: "+temp+" °C<br>"
																+"Humedad: "+hum+" %<br>"
																+"Viento: "+viento+" km/h<br>"
																+"Fecha y Hora de Datos: "+ts
																).openPopup();
													}else{
														L.marker([nodo.latitud,nodo.longitud],{
															icon: icon_peace}).addTo(map).bindPopup(
																"Nodo "+nodo.id+"<br>"
																+"Temperatura: "+temp+" °C<br>"
																+"Humedad: "+hum+" %<br>"
																+"Viento: "+viento+" km/h<br>"
																+"Fecha y Hora de Datos: "+ts
																).openPopup();
													}
												}
											});
										}
									}
								});
							}
						}
					});

					//coger datos meteo de AEMET
					$.ajax({
						url:"https://www.el-tiempo.net/api/json/v2/home",
						type:"get",
						dataType:"JSON",
						success:function(elTiempoNet){
							var codProvincia=null;
							for(i=0;i<elTiempoNet.provincias.length;i++){
								if(elTiempoNet.provincias[i].CAPITAL_PROVINCIA==provincia){
									codProvincia=elTiempoNet.provincias[i].CODPROV;
								}
							}

							$.ajax({
								url:"https://www.el-tiempo.net/api/json/v2/provincias/"+codProvincia+"/municipios",
								type:"get",
								dataType:"JSON",
								success:function(municipios){
									//para la provincia escogida se recogen los ids de los municipios en ella
									var idsMunicipios=[];
									for(i=0;i<municipios.municipios.length;i++){
										idsMunicipios[i]=municipios.municipios[i].COD_GEO;
										$.ajax({
											url:"https://www.el-tiempo.net/api/json/v2/provincias/"+codProvincia+"/municipios/"+idsMunicipios[i],
											type:"get",
											dataType:"JSON",
											success:function(datosMunicipio){
												//put marker for each municipio that the API returns and change icon according to 30/30/30 rule
												var temp=datosMunicipio.temperatura_actual;
												var viento=datosMunicipio.viento;
												var hum=datosMunicipio.humedad;

												//se cumple una de las 3 30s
												if( ((temp>=30)&&(viento<30)&&(hum>=30)) || ((temp<30)&&(viento>=30)&&(hum>=30)) || ((temp<30)&&(viento<30)&&(hum<30)) ){
													L.marker([datosMunicipio.municipio.LATITUD_ETRS89_REGCAN95,datosMunicipio.municipio.LONGITUD_ETRS89_REGCAN95],{
														icon: icon_muni_bajo
													}).addTo(map).bindPopup(
															"Municipio: "+datosMunicipio.municipio.NOMBRE+"<br>"
															+"Temperatura Actual: "+temp+" °C<br>"
															+"Humedad: "+hum+" %<br>"
															+"Viento: "+viento+" km/h<br>"
															+"Lluvia: "+datosMunicipio.lluvia+" %").openPopup();
												//se cumplen 2 de las 3 30s
												}else if( ((temp>=30)&&(viento>=30)&&(hum>=30)) || ((temp<30)&&(viento>=30)&&(hum<30)) || ((temp>=30)&&(viento<30)&&(hum<30)) ){
													L.marker([datosMunicipio.municipio.LATITUD_ETRS89_REGCAN95,datosMunicipio.municipio.LONGITUD_ETRS89_REGCAN95],{
														icon: icon_muni_medio
													}).addTo(map).bindPopup(
															"Municipio: "+datosMunicipio.municipio.NOMBRE+"<br>"
															+"Temperatura Actual: "+temp+" °C<br>"
															+"Humedad: "+hum+" %<br>"
															+"Viento: "+viento+" km/h<br>"
															+"Lluvia: "+datosMunicipio.lluvia+" %").openPopup();
												//se cumplen las 3 30s
												}else if((temp>=30)&&(viento>=30)&&(hum<30)){
													L.marker([datosMunicipio.municipio.LATITUD_ETRS89_REGCAN95,datosMunicipio.municipio.LONGITUD_ETRS89_REGCAN95],{
														icon: icon_muni_alto
													}).addTo(map).bindPopup(
															"Municipio: "+datosMunicipio.municipio.NOMBRE+"<br>"
															+"Temperatura Actual: "+temp+" °C<br>"
															+"Humedad: "+hum+" %<br>"
															+"Viento: "+viento+" km/h<br>"
															+"Lluvia: "+datosMunicipio.lluvia+" %").openPopup();
												}else{
													L.marker([datosMunicipio.municipio.LATITUD_ETRS89_REGCAN95,datosMunicipio.municipio.LONGITUD_ETRS89_REGCAN95],{
														icon: icon_muni_peace
													}).addTo(map).bindPopup(
															"Municipio: "+datosMunicipio.municipio.NOMBRE+"<br>"
															+"Temperatura Actual: "+temp+" °C<br>"
															+"Humedad: "+hum+" %<br>"
															+"Viento: "+viento+" km/h<br>"
															+"Lluvia: "+datosMunicipio.lluvia+" %").openPopup();
												}
												
												//poner un círculo de superficie aproximada para cada municipio que la API devuelve
												L.circle([datosMunicipio.municipio.LATITUD_ETRS89_REGCAN95,datosMunicipio.municipio.LONGITUD_ETRS89_REGCAN95],
													{radius:Math.sqrt(datosMunicipio.municipio.SUPERFICIE*1000/Math.PI),
														color:"#808080",
														weight:2,
														fill:false}
												).addTo(map);
											}
										});
									}
								}
							});
						}
					});
				}
			});
		});	
	</script>



</body>
</html>